import { NgModule } from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ManageuserComponent } from './pages/manageuser/manageuser.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { Footer2Component } from './shared/footer2/footer2.component';
import { HeadercompanyComponent } from './shared/headercompany/headercompany.component';
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { ConfirmationComponent } from './pages/confirmation/confirmation.component';
import { ResetpasswordComponent } from './pages/resetpassword/resetpassword.component';
import { ResetsuccessComponent } from './pages/resetsuccess/resetsuccess.component';
import { ChangepasswordComponent } from './pages/changepassword/changepassword.component';
import { ChangesuccessComponent } from './pages/changesuccess/changesuccess.component';

// Import the module from the SDK
import { AuthModule } from '@auth0/auth0-angular';
// Import the interceptor module and the Angular types you'll need
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthHttpInterceptor } from '@auth0/auth0-angular';
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ManageuserComponent,
    HeaderComponent,
    FooterComponent,
    Footer2Component,
    HeadercompanyComponent,
    RegisterComponent,
    LoginComponent,
    ConfirmationComponent,
    ResetpasswordComponent,
    ResetsuccessComponent,
    ChangepasswordComponent,
    ChangesuccessComponent,
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,  AuthModule.forRoot({
      domain: 'dev-nvk5arub.us.auth0.com',
      clientId: 'DgsVCxMt9vEE2m27pV9ZKtUH7urqkaR7',
    }),
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: AuthHttpInterceptor, multi: true},],
  bootstrap: [AppComponent]
})
export class AppModule { }
