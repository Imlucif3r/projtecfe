import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangesuccessComponent } from './changesuccess.component';

describe('ChangesuccessComponent', () => {
  let component: ChangesuccessComponent;
  let fixture: ComponentFixture<ChangesuccessComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangesuccessComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangesuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
