import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { ChangesuccessComponent } from './pages/changesuccess/changesuccess.component';

// Import the authentication guard
import { AuthGuard } from '@auth0/auth0-angular';
const routes: Routes = [
  {path: '/login',component:LoginComponent ,pathMatch: 'full'},
  {path : '/register', component : RegisterComponent ,pathMatch: 'full'},
  {path : '/dashboard', component : DashboardComponent ,pathMatch: 'full'},
  {path : '/changesuccess', component : ChangesuccessComponent ,pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
