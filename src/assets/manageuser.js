var app = angular.module('MyApp', [])
        app.controller('MyController', function ($scope, $window) {
            $scope.Students = [
                { FirstName: "Nikunj", LastName: "Satasiya", University: "RK University", Branch: "CE", City: "Surat" },
                { FirstName: "Hiren", LastName: "Dobariya", University: "RK University", Branch: "CE", City: "Rajkot" },
                { FirstName: "Vivek", LastName: "Ghadiya", University: "RK University", Branch: "CE", City: "Jamnagar" },
                { FirstName: "Pratik", LastName: "Pansuriya", University: "RK University", Branch: "CE", City: "Rajkot" }
            ];

            $scope.Add = function () {
                //Add the new item to the Array.
                var customer = {};
                customer.FirstName = $scope.FirstName;
                customer.LastName = $scope.LastName;
                customer.University = $scope.University;
                customer.Branch = $scope.Branch;
                customer.City = $scope.City;
                $scope.Students.push(customer);

                //Clear the TextBoxes.
                $scope.Name = "";
                $scope.Country = "";
            };

            $scope.Remove = function (index) {
                //Find the record using Index from Array.
                var name = $scope.Students[index].Name;
                if ($window.confirm("Do you want to delete: " + name)) {
                    //Remove the item from Array using Index.
                    $scope.Students.splice(index, 1);
                }
            }
        });